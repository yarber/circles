﻿using UnityEngine;
using System.Collections;

public class BackGroundSizeScript : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		Vector3 scale = transform.lossyScale;
		
		scale.x = Screen.width/10.0f;
		scale.z = Screen.height/10.0f;
		
		transform.localScale = scale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
