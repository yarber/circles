﻿using UnityEngine;
using System.Collections;

//class to send circle parameters with sockets

[System.Serializable]
public class CircleParameters
{
	//paremeter for action
	[SerializeField]
	public string Command { get; set; }
	
	//game parameters
	[SerializeField]
	public int Time { get; set; }
	
	[SerializeField]
	public int TotalPoints { get; set; }
	
	
	//circle parameters
	[SerializeField]
	public PointParameters Position { get; set; }
	
	[SerializeField]
	public Vector4 Rotation;
	
	[SerializeField]
	public PointParameters Scale { get; set; }
	
	[SerializeField]
	public int ID { get; set; }
	
	[SerializeField]
	public int Size { get; set; }
	
	[SerializeField]
	public float Speed { get; set; }
	
	[SerializeField]
	public int Picture { get; set; }
	
	[SerializeField]
	public int Points { get; set; }

	public CircleParameters(Vector3 position, Quaternion rotation, Vector3 scale, int id, int size, float speed, int picture, int points)
	{
		Position = new PointParameters(position);
		Rotation = new Vector4(rotation);
		Scale = new PointParameters(scale);
		ID = id;
		Size = size;
		Speed = speed;
		Picture = picture;
		Points = points;
	}
}
