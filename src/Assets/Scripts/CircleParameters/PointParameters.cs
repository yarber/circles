﻿using UnityEngine;
using System.Collections;

//class instead of Vector3 for serialization

[System.Serializable]
public class PointParameters
{
	[SerializeField]
	public float X { get; set; }
	
	[SerializeField]
	public float Y { get; set; }
	
	[SerializeField]
	public float Z { get; set; }
	
	public PointParameters(float x, float y, float z)
	{
		X = x;
		Y = y;
		Z = z;
	}
	
	public PointParameters(Vector3 vector)
	{
		X = vector.x;
		Y = vector.y;
		Z = vector.z;
	}
	
	//convert to Vector3
	public Vector3 GetVector3()
	{
		return new Vector3(X, Y, Z);
	}
}
