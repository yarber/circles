﻿using UnityEngine;
using System.Collections;

//class instead of Quaternion for serialization

[System.Serializable]
public class Vector4
{
	[SerializeField]
	public float X { get; set; }
	
	[SerializeField]
	public float Y { get; set; }
	
	[SerializeField]
	public float Z { get; set; }
	
	[SerializeField]
	public float W { get; set; }
	
	public Vector4(float x, float y, float z, float w)
	{
		X = x;
		Y = y;
		Z = z;
		W = w;
	}
	
	public Vector4(Quaternion vector)
	{
		X = vector.x;
		Y = vector.y;
		Z = vector.z;
		W = vector.w;
	}
	
	//convert to quaternion
	public Quaternion GetQuaternion()
	{
		return new Quaternion(X, Y, Z, W);
	}
}
