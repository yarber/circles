﻿using UnityEngine;
using System.Collections;

public delegate void CircleSizeDelegate(Transform circle, int size);

[System.Serializable]
//class for circle logic
public class Circle : MonoBehaviour
{
	//circle size limits
	private int _minSize = 40;
	private int _maxSize = 200;
	
	//speed
	private float _speed;
	
	//scale
	public int Size { get; private set; }
		
	//points for circle
	public int Points { get; private set; }
	
	//speed factor grows with time
	public static float SpeedFactor { get; set; }
	
	//event to know? when circle parameter Size was set
	public event CircleSizeDelegate Sized;
	
	public int ID { get; set; }
	
	//texture id
	public int Picture { get; set; }
	
	public void OnSized(Transform circle, int size)
	{
		if(Sized != null)
			Sized(circle, size);
	}
	
	// Use this for initialization
	void Start ()
	{
		//sset scale, position, speed, texture for circle
		//SetCircleParameters();
	}
	
	// Update is called once per frame
	void Update ()
	{
		MoveDown();
		
		//check if circle is in scene borders
		Vector3 pos = transform.position;

		Rect rect = new Rect(0, 0, Screen.width, Screen.height);
		if(!rect.Contains(Camera.main.WorldToScreenPoint(transform.position)))
			Destroy(gameObject);
	}
	
	//movement
	private void MoveDown()
	{
		//move circle down
		transform.Translate(new Vector3(0, 0, _speed));
	}
	
	//size and position
	public void SetCircleParameters()
	{
		//circle size
		Size = Random.Range(_minSize, _maxSize);
		transform.localScale = new Vector3(Size, Size, Size);

		int textureSize = 0;
		
		//set speed, points, texture size additionally to circle scale
		if(Size < _maxSize/4)
		{
			_speed = 6 * SpeedFactor;
			Points = 10;
			
			textureSize = 32;
		}
		else if(Size < _maxSize/2)
		{
			_speed = 5 * SpeedFactor;
			Points = 5;
			
			textureSize = 64;
		}
		else if(Size < (_maxSize/4)*3)
		{
			_speed = 4 * SpeedFactor;
			Points = 2;
			
			textureSize = 128;
		}
		else if(Size < _maxSize)
		{
			_speed = 3 * SpeedFactor;
			Points = 1;
			
			textureSize = 256;
		}
		
		//call event
		OnSized(transform, textureSize);

		//circle position
		Vector3 pos = Vector3.zero;
		
		float minX = transform.collider.bounds.size.x/2.0f;
		float maxX = Screen.width - transform.collider.bounds.size.x/2.0f;
		pos.x = Random.Range(minX, maxX);

		pos.y = Screen.height - transform.collider.bounds.size.y/2.0f;
		
		pos.z = 200;
		
		transform.position = Camera.main.ScreenToWorldPoint(pos);
	}
	
	//get circle parameters to send them to client
	public CircleParameters GetParameters()
	{
		CircleParameters circleParameters = new CircleParameters(transform.position, transform.rotation, transform.localScale, ID, Size, _speed, Picture, Points);
		
		return circleParameters;
	}
	
	//create circle from parameters
	public void CopyCircleWithParameters(CircleParameters circleParameters)
	{
		transform.position = circleParameters.Position.GetVector3();
		transform.rotation = circleParameters.Rotation.GetQuaternion();
		transform.localScale = circleParameters.Scale.GetVector3();
		
		ID = circleParameters.ID;
		
		Size = circleParameters.Size;
		
		_speed = circleParameters.Speed;
		
		Points = circleParameters.Points;
	}
}
