﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//game state controller
public class GameController : MonoBehaviour
{
	//interval between creating circles
	private float _circleGenerationInterval = 2.5f;
	
	//timer for count time after last circle generation
	private float _circleGenerationTimer = 0;
	
	//player score
	private int _points = 0;
	
	//game time
	private int _gameTime = 0;
	
	//timer for speed up
	private float _speedUpInterval = 20;
	private float _nextSpeedUpTime = 20;
	private float _generationTimeInterval = 0.2f;
	private float _speedFactorInterval = 0.1f;
	
	//textures manager
	private TexturesManager _texturesManager;
	
	private SocketController _socketController;
	private BaseSocket _socket;
	
	private int circlesCounter;
	
	private string _bundlePath;
	private int _version = 1;
	
	private Dictionary<int, GameObject> circles = new Dictionary<int, GameObject>();
	
	// Use this for initialization
	void Start ()
	{
		_bundlePath = "file://" + Application.dataPath + "/AssetBundles/Circle.unity3d";
		
		Circle.SpeedFactor = 1;
		
		_texturesManager = new TexturesManager();
		
		_socketController = (SocketController)GameObject.FindObjectOfType(typeof(SocketController));
		_socket = _socketController.socket;
		
		if(!_socketController.IsServer)
			((ClientSocket)_socket).MesssageRecieved += MessageHandler;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(_socketController.IsServer)
		{
			//count total game time
			_gameTime = (int)Time.realtimeSinceStartup;
			
			//check if need to spped up
			if(_gameTime >= _nextSpeedUpTime)
			{
				_nextSpeedUpTime += _speedUpInterval;
				
				_circleGenerationInterval -= _generationTimeInterval;
				
				Circle.SpeedFactor += _speedFactorInterval;
				
				_texturesManager.Reset();
			}
			
			//measure time for circle generation
			_circleGenerationTimer += Time.deltaTime;
			
			if(_circleGenerationTimer >= _circleGenerationInterval)
			{
				_circleGenerationTimer = 0;

				GenerateCircle();
			}
			
			//click moment
			if(Input.GetMouseButtonDown(0))
			{
				RaycastHit hit;
				
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				
				if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane))
				{
					if(hit.transform != null)
					{
						//check if click was on circle
						if(hit.transform.GetComponent(typeof(Circle)) != null)
						{
							Circle circle = ((Circle)hit.transform.GetComponent(typeof(Circle)));
							
							//get circle size
							int points = circle.Points;
							
							_points += points;
							
							CircleParameters circleParameters = circle.GetParameters();
							circleParameters.Command = "destroy";
							
							((ServerSocket)_socket).SendMessage(circleParameters, _points, _gameTime);
							
							//circles[circle.ID.ToString()]
							if(circles.ContainsKey(circle.ID))
								circles.Remove(circle.ID);
							
							//destroy circle
							Destroy(hit.transform.gameObject);
						}
					}
				}
			}
		}
	}
	
	//create new circle
	private void GenerateCircle()
	{
		StartCoroutine(DownloadAndCache(_bundlePath, _version));
	}
	
	IEnumerator DownloadAndCache(string bundlePath, int version)
	{
		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;

		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using(WWW www = WWW.LoadFromCacheOrDownload (bundlePath, version))
		{
			yield return www;
			
			if (www.error == null)
			{
				AssetBundle bundle = www.assetBundle;
				
				GameObject go = (GameObject)Instantiate(bundle.mainAsset);
				
				Circle circle = ((Circle)go.transform.GetComponent(typeof(Circle)));
				
				circle.ID = ++circlesCounter;
		
				circle.SetCircleParameters();
				
				int picture = -1;
				
				SetCircleTexture(circle.transform, circle.Size, ref picture);
				
				circle.Picture = picture;
				
				CircleParameters circleParameters = circle.GetParameters();
				
				Debug.Log("circleParameters: " + circleParameters.Picture);
				
				if(_socketController.IsServer)
				{
					circleParameters.Command = "create";
					((ServerSocket)_socket).SendMessage(circleParameters, _points, _gameTime);
				}
				
				circles.Add(circle.ID, circle.gameObject);
	
	        	// Unload the AssetBundles compressed contents to conserve memory
	        	bundle.Unload(false);
			}
		}
	}

	public void SetCircleTexture(Transform circle, int size, ref int picture)
	{
		circle.renderer.material.mainTexture = _texturesManager.GenerateTexture(size, ref picture);
	}
	
	//show points and game time
	void OnGUI()
	{
		GUI.Label(new Rect(10, Screen.height - 100, 60, 50), "Points: ");
		GUI.Label(new Rect(60, Screen.height - 100, Screen.width - 60, 50), _points.ToString());
		
		GUI.Label(new Rect(10, Screen.height - 50, 50, 50), "Time: ");
		GUI.Label(new Rect(60, Screen.height - 50, Screen.width - 60, 50), _gameTime.ToString());
		
		if(!_socketController.IsServer)
		{
			GUILayout.Label(ClientSocket.ololo);
		}
	}

	//class to invoke, when message from server recieved
	public void MessageHandler(CircleParameters circleParameters)
	{
		//choose create or destroy circle
		if(circleParameters.Command == "create")
			GenerateCircle(circleParameters);
		else if(circleParameters.Command == "destroy")
		{
			if(circles.ContainsKey(circleParameters.ID))
			{
				DestroyObject(circles[circleParameters.ID]);
				
				circles.Remove(circleParameters.ID);
			}
		}
		
		//set game parameters
		if(_points != circleParameters.TotalPoints)
			_points = circleParameters.TotalPoints;
		if(_gameTime != circleParameters.Time)
			_gameTime = circleParameters.Time;
	}
	
	//create new circle with parameteres, recieved from server
	private void GenerateCircle(CircleParameters circleParameters)
	{
		GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				
		go.AddComponent(typeof(Circle));
		
		Circle circle = ((Circle)go.transform.GetComponent(typeof(Circle)));
				
		circle.CopyCircleWithParameters(circleParameters);
		
		int picture = circleParameters.Picture;
		
		SetCircleTexture(circle.transform, circle.Size, ref picture);
		
		circles.Add(circle.ID, circle.gameObject);
	}
}
