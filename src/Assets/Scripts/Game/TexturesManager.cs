﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//manager for circles dynamic textures
public class TexturesManager
{
	// dictionaries for textures with different sizes
	//one dictionary for every picture
	private Dictionary<int, Texture2D> textureUkraine = new Dictionary<int, Texture2D>();
	private Dictionary<int, Texture2D> textureBelarus = new Dictionary<int, Texture2D>();
	private Dictionary<int, Texture2D> texturePoland = new Dictionary<int, Texture2D>();
	
	//generate texture for circle
	public Texture2D GenerateTexture(int size, ref int picture)
	{
		//choose picture
		if(picture < 0 || picture > 2)
			picture = Random.Range(0, 3);
		
		if(picture == 0)
		{
			if(textureUkraine.ContainsKey(size))
				return textureUkraine[size];
		}
		else if(picture == 1)
		{
			if(textureBelarus.ContainsKey(size))
				return textureBelarus[size];
		}
		else if(picture == 2)
		{
			if(texturePoland.ContainsKey(size))
				return texturePoland[size];
		}
		
		//create texture
		Texture2D texture = new Texture2D(size, size, TextureFormat.ARGB32, false);
		
		Color color = Color.clear;
		Color colorTop = Color.clear;
		Color colorBottom = Color.clear;
		
		//choose colors
		if(picture == 0)
		{
			colorTop = Color.blue;
			colorBottom = Color.yellow;
			
			textureUkraine.Add(size, texture);
		}
		else if(picture == 1)
		{
			colorTop = Color.red;
			colorBottom = Color.green;
			
			textureBelarus.Add(size, texture);
		}
		else if(picture == 2)
		{
			colorTop = Color.white;
			colorBottom = Color.red;
			
			texturePoland.Add(size, texture);
		}
		
		//set each pizel to chosen color
		for(int i = 0; i < size; i++)
		{
			if(i < size/2)
				color = colorTop;
			else
				color = colorBottom;
			
			for(int j = 0; j < size; j++)
			{
				texture.SetPixel(i, j, color);
			}
		}
		
		//apply changes to texture
		texture.Apply();
		
		Debug.Log("picture before return: " + picture);
		
		return texture;
	}
	
	//clear textures with speed up
	public void Reset()
	{
		textureUkraine.Clear();
		textureBelarus.Clear();
		texturePoland.Clear();
		
		Resources.UnloadUnusedAssets();
	}
}
