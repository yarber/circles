﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour
{
	public SocketController socketController;
	
	private string _ip;
	
	// Use this for initialization
	void Start ()
	{
		_ip = "localhost";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI()
	{
		_ip = GUI.TextField(new Rect(Screen.width/2 - 150, Screen.height/2 - 200, 300, 25), _ip);
		
		//button to play
		if(GUI.Button(new Rect(Screen.width/2 - 50, Screen.height/2 - 150, 100, 100), "Play"))
		{
			socketController.IsServer = true;
			
			socketController.Init(_ip);
			
			Application.LoadLevel("GameScene");
		}
		
		//button to watch anothe game
		if(GUI.Button(new Rect(Screen.width/2 - 50, Screen.height/2 + 50, 100, 100), "Connect"))
		{
			socketController.IsServer = false;
			
			socketController.Init(_ip);
			
			Application.LoadLevel("GameScene");
		}
	}
}
