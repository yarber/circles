﻿using UnityEngine;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System;

//base socke type
public class BaseSocket : IDisposable
{
	protected Thread _thread;
	
	protected Socket _socket;
	
	public virtual void Dispose()
	{
	}
}
