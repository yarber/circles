﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

public delegate void ClienDelegate(CircleParameters circleParameters);

public class ClientSocket : BaseSocket, IDisposable
{
	public static string ololo;
	
	public event ClienDelegate MesssageRecieved;
	
	private string _ip;
	
	private void OnMesssageRecieved(CircleParameters circleParameters)
	{
		if(MesssageRecieved != null)
			MesssageRecieved(circleParameters);
	}
	
	//socket works in new thread
	public ClientSocket(string ip)
	{
		_ip = ip;
		
		_thread = new Thread(ConnectToServer);
		_thread.Start();
	}
	
	//connect to server
	public void ConnectToServer()
	{
		int port = 11000;

        //connect point to socket
        //IPHostEntry ipHost = Dns.GetHostEntry("localhost");
		IPHostEntry ipHost = Dns.GetHostEntry(_ip);
        IPAddress ipAddr = ipHost.AddressList[0];
        IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);
		
		_socket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
		
		//connect socket
        _socket.Connect(ipEndPoint);
		
		//recieve message
		RecieveMessage();
	}
	
	//get message from server
	private void RecieveMessage()
	{
		Debug.Log("RecieveMessage()");
		
		//buffer for data
        byte[] bytes = new byte[1024];
		
		//get data from server
        int bytesRec = _socket.Receive(bytes);
		
		Deserialize(bytes);
		
		RecieveMessage();
	}
	
	//close connections
	public override void Dispose()
	{
		try
		{
			_socket.Shutdown(SocketShutdown.Both);
			_socket.Close();
		}
		catch(Exception ex)
		{
			Debug.Log("Dispose server error: " + ex.Message);
		}
		
		if(_thread != null)
			_thread.Abort();
	}
	
	//deserialize circle and game parameters
	private void Deserialize(byte[] buffer)
	{
		BinaryFormatter bf = new BinaryFormatter();
		
		using(MemoryStream stream = new MemoryStream(buffer))
		{
            CircleParameters circleParameters = (CircleParameters)bf.Deserialize(stream);
			
			UnityFramework.Threading.UnitySynchronizationContext.Current.Post((obj)=>
			{
				OnMesssageRecieved((CircleParameters)obj);
			}, circleParameters);
		}
	}
}
