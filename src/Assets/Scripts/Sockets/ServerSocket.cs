﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

public class ServerSocket : BaseSocket, IDisposable
{
	//list of client sockets
	List<Socket> _handlers = new List<Socket>();
	
	private string _ip;
	
	//socket works in new thread
	public ServerSocket(string ip)
	{
		_ip = ip;
		
		_thread = new Thread(Listen);
		_thread.Start();
	}
	
	//wait for connections
	private void Listen()
	{
		//set local end poit fo socket
		//Dns.Get
		
		//IPHostEntry ipHost = Dns.GetHostEntry("localhost");
		IPHostEntry ipHost = Dns.GetHostEntry(_ip);
		IPAddress ipAddr = ipHost.AddressList[0];
		IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);
		
		//create TCP/IP socket
		_socket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
		
		//set socket to local end point and listen income sockets
		try
		{
			_socket.Bind(ipEndPoint);
			_socket.Listen(10);
			
			//begin to listen
			while(true)
			{
                Socket handler = _socket.Accept();
				
				_handlers.Add(handler);
			}
		}
		catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
	}
	
	//send message for connected sockets
	public void SendMessage(CircleParameters circleParameters, int points, int time)
	{
		circleParameters.TotalPoints = points;
		circleParameters.Time = time;
		
		if(_handlers != null)
		{
			byte[] buffer = Serialize(circleParameters);
			
			foreach(Socket handler in _handlers)
			{
				try
				{
					//send data with socket
        			int bytesSent = handler.Send(buffer);
				}
				catch(Exception ex)
				{
					Debug.Log("sending message error: " + ex.Message);
				}
			}
		}
	}
	
	//close connections
	public override void Dispose()
	{
		if(_handlers != null)
		{
			foreach(Socket handler in _handlers)
			{
				try
				{
					handler.Shutdown(SocketShutdown.Both);
					handler.Close();
				}
				catch(Exception ex)
				{
					Debug.Log("Dispose server error: " + ex.Message);
				}
			}
		}
		
		if(_socket != null)
			_socket.Close();
		
		if(_thread != null)
			_thread.Abort();
	}
	
	//serialize circle and game parameters
	private byte[] Serialize(CircleParameters circleParameters)
	{
		byte[] buffer = null;
		
		BinaryFormatter bf = new BinaryFormatter();
 
		using(MemoryStream stream = new MemoryStream())
		{
        	bf.Serialize(stream, circleParameters);
			
			buffer = stream.ToArray();
		}
		
		return buffer;
	}
}
