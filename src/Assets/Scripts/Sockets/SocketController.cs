﻿using UnityEngine;
using System.Collections;

public class SocketController : MonoBehaviour
{
	//client\server flag
	public bool IsServer { get; set; }
	
	public BaseSocket socket { get; private set; }
	
	private string _ip;
	
	void Awake()
	{
		DontDestroyOnLoad(this);
	}
	
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Init(string ip)
	{
		//choose socket type
		if(IsServer)
			socket = new ServerSocket(ip);
		else
			socket = new ClientSocket(ip);
	}
	
	void OnDestroy()
	{
		if(socket != null)
			socket.Dispose();
	} 
}
