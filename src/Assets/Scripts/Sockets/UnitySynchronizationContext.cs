using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Threading;

namespace UnityFramework.Threading
{
	public class UnitySynchronizationContext : MonoBehaviour 
	{
		//callback information for queue
	    private class CallbackInfo
	    {
	        public SendOrPostCallback Callback { get; private set; }
	        public object AsyncState { get; private set; }
	
	        public CallbackInfo(SendOrPostCallback callback, object asyncState)
	        {
	            Callback = callback;
	            AsyncState = asyncState;
	        }
	
	        public void Invoke()
	        {
	            if (Callback != null)
	                Callback.Invoke(AsyncState);
	        }
	    }
	
		
		//callbacks queue
	    private Queue<CallbackInfo> _queue = new Queue<CallbackInfo>();
		
		//singleton
		private static UnitySynchronizationContext _current = null;
		public static UnitySynchronizationContext Current { get { return _current; } }
		
		
		private void Awake()
		{
			_current = this;
		}
		
		private void OnDestroy()
		{
			if (_current == this)
				_current = null;
		}
		
		//add callback to queue
	    public void Post(SendOrPostCallback d)
	    {
	        Post(d, null);
	    }
	
		//add callback to queue
	    public void Post(SendOrPostCallback d, object state)
	    {
	        lock (_queue)
	        {
	            _queue.Enqueue(new CallbackInfo(d, state));
	        }
	    }
	
		//checking for invoke callback from queue
	    private void Update() 
	    {
	        lock (_queue)
	        {
	            int processed = 0;
	            while (_queue.Count > 0 && processed < 10)
	            {
	                CallbackInfo callbackInfo = _queue.Dequeue();
	                callbackInfo.Invoke();
	
	                processed++;
	            }
	        }
		}
	}
}